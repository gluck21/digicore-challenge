import java.util.Arrays;

public class TableTennis {
    public static int[] tennisScore (int[] firstPlayer, int[] secondPlayer) {
        int[] finalScore = new int[2];
        int first = 0;
        int second = 0;
        for (int i = 0; i < firstPlayer.length; i++) {
            if (firstPlayer[i] > secondPlayer[i]) {
                first++;
            } else if (firstPlayer[i] < secondPlayer[i]) {
                second++;
            }
        }
        finalScore[0] = first;
        finalScore[1] = second;
        System.out.println(Arrays.toString(finalScore));
        return finalScore;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(tennisScore(new int[]{1, 4, 7, 2, 4}, new int[]{3, 4, 2, 4, 4})));
    }
}
